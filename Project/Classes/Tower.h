//#ifndef TOWER_H
//#define TOWER_H
#pragma once

#include "cocos2d.h"
#include "GameObject.h"
#include "Projectile.h"
//need enemy class

using namespace cocos2d;

class Tower : public CGameObject
{
public:
	enum TowerType
	{
		NORMAL = 0,
		MACHINE_GUN,
		SAM_SITE,
		TOWER_TYPE_TOTAL,
	};
	enum TowerAttackType
	{
		NONE = -1,
		// Ground
		CAN_ATTACK_GROUND = 0,
		CAN_ATTACK_AIR,
		CAN_HOME_ENEMY,
		CAN_EXPLODE_ENEMY,
		TOWER_ATTACK_TYPE_TOTAL,
	};
	Tower();
	virtual ~Tower();

	void setStats(float Range, float ROF,float dam);
	void setAttackType(TowerAttackType _first);//, TowerAttackType _second = NONE, TowerAttackType _third = NONE, TowerAttackType _fourth = NONE);

	float getRange();
	float getROF();
	bool getCanShoot();
	bool getAttackType(TowerAttackType _type);
	TowerType getTowerType();
	void goOnCooldown();
	//float getDam();
	
	Sprite* sprite;

	virtual bool init(TowerType type = NORMAL);
	virtual Tower* create(const std::string& file);

	void update(float delta);

	void draw(Renderer * renderer, const Mat4& transform, uint32_t flags);
	//bool isEnemyInRange()


	CGameObject* target;

	Sprite* selSpriteRange;

protected:
	int Range;
	int Damage;
	int TowerCost;

	float RateOfFire;
	float originalRateOfFire;
	float turnAngle;
	
	bool canShoot;
	TowerAttackType AttackType[TOWER_ATTACK_TYPE_TOTAL];
	TowerType towerType;

	Vec2 TowerScale;
	
};


//#endif // !TOWER_H

