#ifndef _ENEMY_H_
#define _ENEMY_H_

#include "cocos2d.h"
#include "Waypoint.h"
#include "GameObject.h"
#include "WaypointManager.h"
#include "GameManager.h"
#include <vector>

using namespace cocos2d;

class CEnemy : public CGameObject
{
protected:
	int initialHealth;
	int health;
	int maxHealth;
	float speed;
	int goldReward;
	int waypointID;
	bool destroyed;
	bool hasReachedEnd;
	Size visibleSize;

#pragma region Temp stuffs
	/*Vector<SpriteFrame*> animFrames;
	Animation* animation;
	SpriteBatchNode* spritebatch;
	SpriteFrameCache* cache;*/
#pragma endregion

	// test
	int waypointNum[11];
	Label* hpLabel;
	//int curWaypoint;
	//int tag;
	//std::string fileName;

public:
	CEnemy();
	virtual ~CEnemy();
	
	Sprite* sprite;
	Sprite* hpBar;
	//CObjectPool* objectPool;
	CWaypointManager cWaypointManager;
	CGameManager* gm;
	
	virtual bool init();
	//CREATE_FUNC(CEnemy);
	virtual CEnemy* create(const std::string& file);
	CEnemy* initWithEnemy(CEnemy* _enemy);

	int GetWaypointID();

	void SetWaypointID(int _waypointID);
	void DamageEnemy(int _dmg);
	void SetWaypoints();
	void setData(Vec2 spawnLoc);
	int GetHealth();
	int GetGoldReward();
	int SetHealth(int waveNo);

	virtual void SetDestroyed(bool _value);
	virtual bool GetDestroyed();
	virtual void SetHasReachedEnd(bool _value);
	virtual bool GetHasReachedEnd();

	virtual void GoToNextWaypoint();

	virtual void update(float delta);

	void draw(Renderer * renderer, const Mat4 & transform, uint32_t flags);
	
	//virtual void update(float delta);
};

class CGroundEnemy : public CEnemy
{
public:
	CGroundEnemy();
	~CGroundEnemy();
	bool init();
	//CEnemy* create(const std::string& file);
};

class CGroundBoss : public CGroundEnemy
{
public:
	CGroundBoss();
	~CGroundBoss();
	bool init();
	//CEnemy* create(const std::string& file);
};

class CFlyingEnemy : public CEnemy
{
public:
	CFlyingEnemy();
	~CFlyingEnemy();
	bool init();
	//CEnemy* create(const std::string& file);
};

class CFlyingBoss : public CFlyingEnemy
{
public:
	CFlyingBoss();
	~CFlyingBoss();
	bool init();
	//CEnemy* create(const std::string& file);
};
#endif