#include "GameObject.h"

//CGameObject::CGameObject(GO_TYPE typeValue)
//	: type(typeValue),
//	sprite(NULL)
//{
//
//}

CGameObject::CGameObject(GO_TYPE typeValue)
	: type(typeValue),
	sprite(NULL)
{
}

CGameObject::~CGameObject()
{
}

void CGameObject::update(float dt)
{
}

void CGameObject::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
	sprite->draw(renderer, transform, flags);
}