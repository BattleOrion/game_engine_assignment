#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_

#include "cocos2d.h"

using namespace cocos2d;

class CGameObject : public Sprite
{
	
public:
	enum GO_TYPE
	{
		GO_NONE = 0,
		GO_PROJECTILE,
		GO_ENEMYGROUND,
		GO_ENEMYFLYING,
		GO_BOSSGROUND,
		GO_BOSSFLYING,
		GO_TOWER,

	} type;

	Sprite* sprite;
	//CGameObject();
	CGameObject(GO_TYPE typeValue = GO_NONE);
	~CGameObject();

	virtual void update(float dt);

	virtual void draw(Renderer * renderer, const Mat4 & transform, uint32_t flags);
	
	
};

#endif // !_GAMEOBJECT_H_

