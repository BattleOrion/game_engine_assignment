#include "Waypoint.h"
using namespace cocos2d;
using namespace std;

CWaypoint::CWaypoint()
	: position(Vec2::ZERO)
	, m_iID(1)
{
	// Init this class instance
	Init();
}

CWaypoint::~CWaypoint()
{
	// Destroy this class instance
	Destroy();
}

// Init this class instance
void CWaypoint::Init(void)
{
	// Clear the relatedWaypoints
	relatedWaypoints.clear();
}

// Destroy this class instance
void CWaypoint::Destroy(void)
{
	// Clear the relatedWaypoints
	relatedWaypoints.clear();
}

// Set position
void CWaypoint::SetPosition(Vec2 position)
{
	this->position = position;
}

// Get position
Vec2 CWaypoint::GetPosition(void) const
{
	return position;
}

// Set ID
void CWaypoint::SetID(const int m_iID)
{
	this->m_iID = m_iID;
}

// Get ID
int CWaypoint::GetID(void) const
{
	return m_iID;
}

// Add related waypoint
void CWaypoint::AddRelatedWaypoint(CWaypoint * relatedWaypoint)
{
	relatedWaypoints.push_back(relatedWaypoint);
}

bool CWaypoint::RemoveRelatedWaypoint(const int m_iID)
{
	// If Waypoints has related Waypoints, then we proceed to search
	if (relatedWaypoints.size() != 0)
	{
		// Iterate through all the Waypoints
		std::vector<CWaypoint*>::iterator it = relatedWaypoints.begin();
		while (it != relatedWaypoints.end())
		{
			if ((*it)->GetID() == m_iID)
			{
				// We erase from the vector, relatedWaypoints only. DO NOT DELETE!
				// Let CWaypointManager delete it
				it = relatedWaypoints.erase(it);
				return true;
			}
			++it;
		}
	}
	return false;
}

CWaypoint * CWaypoint::GetNearestWaypoint(void)
{
	CWaypoint* theNearestWaypoint = NULL;
	float m_fDistance = numeric_limits<float>::max();
	// If Waypoints has related Waypoints, then we proceed to search.
	if (relatedWaypoints.size() != 0)
	{
		// Iterate through all the Waypoints
		std::vector<CWaypoint*>::iterator it = relatedWaypoints.begin();
		while (it != relatedWaypoints.end())
		{
			// Get position of a WayPoint
			Vec2 aRelatedWaypoint = (*it)->GetPosition();
			// Get the x- and z-component distance between position and aRelatedWaypoint
			float xDistance = position.x - aRelatedWaypoint.x;
			float yDistance = position.y - aRelatedWaypoint.y;
			// Calculate the distance squared between position and aRelatedWaypoint
			float distanceSquared = (float)(xDistance*xDistance + yDistance * yDistance);
			if (m_fDistance > distanceSquared)
			{
				// Set distanceSquared to m_fDistance
				m_fDistance = distanceSquared;
				// Set theNearestWaypoint to this WayPoint
				theNearestWaypoint = (CWaypoint*)(*it);
			}
			// Increment the iterator
			++it;
		}
	}
	// Return the nearest WayPoint
	return theNearestWaypoint;
}

// Get number of related Waypoints
int CWaypoint::GetNumberOfWaypoints(void) const
{
	return relatedWaypoints.size();
}
