/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "ui/CocosGUI.h"

#define FIRST_TOWER_POSITION 100
#define ORIGINAL_TOUCH_TIMER 2

USING_NS_CC;

Scene* HelloWorld::createScene()
{
	return HelloWorld::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
	printf("Error while loading: %s\n", filename);
	printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	// for testing only
	Size testingsize = Size(1024, 768);
	playingSize = Size(testingsize.width, testingsize.height);

	// Add sprites into screen
	auto testSprite = Sprite::create("Map 1test.png");
	testSprite->setAnchorPoint(Vec2(0, 0));
#ifdef __APPLE__
    testSprite->setPosition(0, 80);
    testSprite->setScale(1.0f, 0.75f);
#endif

#ifdef __ANDROID__
    testSprite->setPosition(0, 20);
    testSprite->setScale(2.0f, 1.2f);
#endif
	this->addChild(testSprite, 1);
	testSprite = Sprite::create("towerDefense_tile024A.png");
	spriteW = testSprite->getContentSize().width;
	spriteH = testSprite->getContentSize().height;

	auto sprite = Sprite::create("HelloWorld.png");
	sprite->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));

	// build menu
	isHeldDown = false;
	touchtimer = ORIGINAL_TOUCH_TIMER;
	dragging = false;
	// rangefinder
	auto mainSprite = Sprite::create("ZigzagGrass_Mud_Round.png");
	mainSprite->setColor(Color3B::YELLOW);
	mainSprite->setOpacity(70);
	mainSprite->setScale(0);
	mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
	mainSprite->setPosition(Vec2(0, 0));
	mainSprite->setName("RangeFinder");
	this->addChild(mainSprite, 9);

	//auto enemy = (CEnemy*)Sprite::create("White_Front1.png");
	//enemy->setName("Enemy");
	////enemy->cWaypointManager.Init();
	///*enemy->SetWaypointID(cWaypointManager.AddWaypoint(Vec2(visibleSize.width / 2, 0)));
	//enemy->SetWaypointID(cWaypointManager.AddWaypoint(enemy->GetWaypointID(), Vec2(visibleSize.width / 2, visibleSize.height / 2)));
	//enemy->SetWaypointID(cWaypointManager.AddWaypoint(enemy->GetWaypointID(), Vec2(0, visibleSize.height / 2)));*/
	////enemy->setPosition(enemy->cWaypointManager.GetWaypoint(0)->GetPosition());
	//this->addChild(enemy, 10);

	if (gm == NULL)
		gm = new CGameManager();

	timeCount = 0;
	timer = 1;
	//waveNum = 1;
	//maxLives = 5;
	gm->instance()->SetParameters();
	//gm->instance()->SetWaveNum(0);
	curGold = 500;

	maxLivesLabel = Label::createWithTTF("", "fonts/Marker Felt.ttf", 24);
	waveNumLabel = Label::createWithTTF("", "fonts/Marker Felt.ttf", 24);
	waveDelayLabel = Label::createWithTTF("", "fonts/Marker Felt.ttf", 60);
	goldLabel = Label::createWithTTF("", "fonts/Marker Felt.ttf", 24);

#ifdef __APPLE__
    if (maxLivesLabel != nullptr)
        maxLivesLabel->setPosition(visibleSize.width * 0.5, visibleSize.height - 20);
    
    if (waveNumLabel != nullptr)
        waveNumLabel->setPosition(visibleSize.width * 0.5, visibleSize.height - 50);
    
    if (waveDelayLabel != nullptr)
        waveDelayLabel->setPosition(visibleSize.width * 0.5, visibleSize.height * 0.5);
    
    if (goldLabel != nullptr)
        goldLabel->setPosition(visibleSize.width - 100, 100);
   
#elif __ANDROID__
    if (maxLivesLabel != nullptr)
        maxLivesLabel->setPosition(visibleSize.width * 0.5, visibleSize.height - 70);
    
    if (waveNumLabel != nullptr)
        waveNumLabel->setPosition(visibleSize.width * 0.5, visibleSize.height - 100);
    
    if (waveDelayLabel != nullptr)
        waveDelayLabel->setPosition(visibleSize.width * 0.5, visibleSize.height * 0.5);
    
    if (goldLabel != nullptr)
        goldLabel->setPosition(visibleSize.width - 100, -100);
#else
    if (maxLivesLabel != nullptr)
        maxLivesLabel->setPosition(visibleSize.width * 0.5, visibleSize.height - 100);
    
    if (waveNumLabel != nullptr)
        waveNumLabel->setPosition(visibleSize.width * 0.5, visibleSize.height - 130);

    if (waveDelayLabel != nullptr)
        waveDelayLabel->setPosition(visibleSize.width * 0.5, visibleSize.height * 0.5);
    
    if (goldLabel != nullptr)
        goldLabel->setPosition(visibleSize.width - 100, -20);
#endif
    this->addChild(maxLivesLabel, 15);
    this->addChild(waveNumLabel, 15);
    this->addChild(waveDelayLabel, 15);
    this->addChild(goldLabel, 15);
	//auto listener1 = EventListenerMouse::create();
	//listener1->onMouseMove = CC_CALLBACK_1(HelloWorld::onMouseMove, this);
	//listener1->onMouseUp = CC_CALLBACK_1(HelloWorld::onMouseUp, this);

#ifdef _WINDOWS_
	// For Windows
	auto listenerMouse = EventListenerMouse::create();
	listenerMouse->onMouseMove = CC_CALLBACK_1(HelloWorld::onMouseMove, this);
	listenerMouse->onMouseUp = CC_CALLBACK_1(HelloWorld::onMouseUp, this);
	listenerMouse->onMouseDown = CC_CALLBACK_1(HelloWorld::onMouseDown, this);
	//listenerMouse->onMouseScroll = CC_CALLBACK_1(HelloWorld::onMouseScroll, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listenerMouse, this);
#else
	// For Android or APPLE
	auto listenerTouch = EventListenerTouchOneByOne::create();
	listenerTouch->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
	listenerTouch->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);
	listenerTouch->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listenerTouch, this);
#endif // __ANDROID__

	audio = CocosDenshion::SimpleAudioEngine::getInstance();

	BuildMenuOpen = false;

	this->scheduleUpdate();
	return true;
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
	//Close the cocos2d-x game scene and quit the application
	Director::getInstance()->end();

	/*To navigate back to native APPLE screen(if present) without quitting the application  ,do not use Director::getInstance()->end() as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);


}

void HelloWorld::update(float delta)
{
	if (test == NULL)
	{
		test = new CObjectPool();
		test->instance()->initObjectPool();
	}

	if (cam == NULL)
	{
		cam = Camera::getDefaultCamera();
		cam->createOrthographic(1024, 768, -10, 100);
#ifdef __ANDROID__
		cam->setPositionY(cam->getPositionY() - 175);
#else
		cam->setPositionY(cam->getPositionY() - 50);
#endif
		OriginalCameraPosition = cam->getPosition();
	}

	// build menu position
#ifdef __ANDROID__
	Vec2 worldposition = Vec2(cam->getPositionX() - OriginalCameraPosition.x, cam->getPositionY() - OriginalCameraPosition.y);
	if (!BuildMenuOpen)
	{

		//if (mouseposition.y <= )
		// Tower 1
		auto mainSprite = Sprite::create("towerDefense_tile024A.png");
		mainSprite->setColor(Color3B::RED);
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 100, worldposition.y + 30);
		mainSprite->setName("TestBlock");
		this->addChild(mainSprite, 11);

		mainSprite = Sprite::create("towerDefense_tile134.png");
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 100, worldposition.y + 30);
		mainSprite->setName("TestTower");
		this->addChild(mainSprite, 12);

		//Tower 2
		mainSprite = Sprite::create("towerDefense_tile024A.png");
		mainSprite->setColor(Color3B::RED);
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 200, worldposition.y + 30);
		mainSprite->setName("TestBlock2");
		this->addChild(mainSprite, 11);

		mainSprite = Sprite::create("towerDefense_tile249.png");
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 200, worldposition.y + 30);
		mainSprite->setName("TestTower2");
		this->addChild(mainSprite, 12);

		// Tower 3
		mainSprite = Sprite::create("towerDefense_tile024A.png");
		mainSprite->setColor(Color3B::RED);
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 300, worldposition.y + 30);
		mainSprite->setName("TestBlock3");
		this->addChild(mainSprite, 11);

		mainSprite = Sprite::create("towerDefense_tile206.png");
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 300, worldposition.y + 30);
		mainSprite->setName("TestTower3");
		this->addChild(mainSprite, 12);

		BuildMenuOpen = true;
	}
	else
	{
		// gold cost
		// if (player->gold <= costNormal)
		auto mainSprite = this->getChildByName("TestBlock");
		mainSprite->setPosition(worldposition.x + 100, worldposition.y + 30);
		/*mainSprite = this->getChildByName("TestTower");
		mainSprite->setPosition(worldposition.x + 100, worldposition.y +30);*/
		mainSprite = this->getChildByName("TestBlock2");
		mainSprite->setPosition(worldposition.x + 200, worldposition.y + 30);
		//mainSprite = this->getChildByName("TestTower2");
		//mainSprite->setPosition(worldposition.x + 200, worldposition.y + 30);
		mainSprite = this->getChildByName("TestBlock3");
		mainSprite->setPosition(worldposition.x + 300, worldposition.y + 30);
	}
	// build menu position

#elif __APPLE__
	Vec2 worldposition = Vec2(cam->getPositionX() - OriginalCameraPosition.x, cam->getPositionY() - OriginalCameraPosition.y);
	if (!BuildMenuOpen)
	{

		//if (mouseposfition.y <= )
		// Tower 1
		auto mainSprite = Sprite::create("towerDefense_tile024A.png");
		mainSprite->setColor(Color3B::RED);
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 100, worldposition.y + 80);
		mainSprite->setName("TestBlock");
		this->addChild(mainSprite, 11);

		mainSprite = Sprite::create("towerDefense_tile134.png");
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 100, worldposition.y + 80);
		mainSprite->setName("TestTower");
		this->addChild(mainSprite, 12);

		//Tower 2
		mainSprite = Sprite::create("towerDefense_tile024A.png");
		mainSprite->setColor(Color3B::RED);
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 200, worldposition.y + 80);
		mainSprite->setName("TestBlock2");
		this->addChild(mainSprite, 11);

		mainSprite = Sprite::create("towerDefense_tile249.png");
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 200, worldposition.y + 80);
		mainSprite->setName("TestTower2");
		this->addChild(mainSprite, 12);

		// Tower 3
		mainSprite = Sprite::create("towerDefense_tile024A.png");
		mainSprite->setColor(Color3B::RED);
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 300, worldposition.y + 80);
		mainSprite->setName("TestBlock3");
		this->addChild(mainSprite, 11);

		mainSprite = Sprite::create("towerDefense_tile206.png");
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 300, worldposition.y + 80);
		mainSprite->setName("TestTower3");
		this->addChild(mainSprite, 12);

		BuildMenuOpen = true;
	}
	else
	{
		// gold cost
		// if (player->gold <= costNormal)
		auto mainSprite = this->getChildByName("TestBlock");
		mainSprite->setPosition(worldposition.x + 100, worldposition.y + 80);
		/*mainSprite = this->getChildByName("TestTower");
		mainSprite->setPosition(worldposition.x + 100, worldposition.y +30);*/
		mainSprite = this->getChildByName("TestBlock2");
		mainSprite->setPosition(worldposition.x + 200, worldposition.y + 80);
		//mainSprite = this->getChildByName("TestTower2");
		//mainSprite->setPosition(worldposition.x + 200, worldposition.y + 30);
		mainSprite = this->getChildByName("TestBlock3");
		mainSprite->setPosition(worldposition.x + 300, worldposition.y + 80);
		//mainSprite = this->getChildByName("TestTower3");
		//mainSprite->setPosition(worldposition.x + 300, worldposition.y + 30);
	}
#else
	Vec2 worldposition = Vec2(cam->getPositionX() - OriginalCameraPosition.x, cam->getPositionY() - OriginalCameraPosition.y);
	if (!BuildMenuOpen)
	{

		//if (mouseposition.y <= )
		// Tower 1
		auto mainSprite = Sprite::create("towerDefense_tile024A.png");
		mainSprite->setColor(Color3B::RED);
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 100, worldposition.y + 30);
		mainSprite->setName("TestBlock");
		this->addChild(mainSprite, 11);

		mainSprite = Sprite::create("towerDefense_tile134.png");
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 100, worldposition.y + 30);
		mainSprite->setName("TestTower");
		this->addChild(mainSprite, 12);

		//Tower 2
		mainSprite = Sprite::create("towerDefense_tile024A.png");
		mainSprite->setColor(Color3B::RED);
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 200, worldposition.y + 30);
		mainSprite->setName("TestBlock2");
		this->addChild(mainSprite, 11);

		mainSprite = Sprite::create("towerDefense_tile249.png");
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 200, worldposition.y + 30);
		mainSprite->setName("TestTower2");
		this->addChild(mainSprite, 12);

		// Tower 3
		mainSprite = Sprite::create("towerDefense_tile024A.png");
		mainSprite->setColor(Color3B::RED);
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 300, worldposition.y + 30);
		mainSprite->setName("TestBlock3");
		this->addChild(mainSprite, 11);

		mainSprite = Sprite::create("towerDefense_tile206.png");
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x + 300, worldposition.y + 30);
		mainSprite->setName("TestTower3");
		this->addChild(mainSprite, 12);

		// Tower 4
		mainSprite = Sprite::create("towerDefense_tile024A.png");
		mainSprite->setColor(Color3B::RED);
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(worldposition.x, worldposition.y);
		mainSprite->setName("TestBlock4");
		this->addChild(mainSprite, 11);

		mainSprite = Sprite::create("towerDefense_tile206.png");
		mainSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		mainSprite->setPosition(cam->getPositionX(), cam->getPositionY());
		mainSprite->setName("TestTower4");
		this->addChild(mainSprite, 12);

		BuildMenuOpen = true;
	}
	else
	{
		// gold cost
		// if (player->gold <= costNormal)
		auto mainSprite = this->getChildByName("TestBlock");
		mainSprite->setPosition(worldposition.x + 100, worldposition.y + 30);
		/*mainSprite = this->getChildByName("TestTower");
		mainSprite->setPosition(worldposition.x + 100, worldposition.y +30);*/
		mainSprite = this->getChildByName("TestBlock2");
		mainSprite->setPosition(worldposition.x + 200, worldposition.y + 30);
		//mainSprite = this->getChildByName("TestTower2");
		//mainSprite->setPosition(worldposition.x + 200, worldposition.y + 30);
		mainSprite = this->getChildByName("TestBlock3");
		mainSprite->setPosition(worldposition.x + 300, worldposition.y + 30);
		//mainSprite = this->getChildByName("TestTower3");
		//mainSprite->setPosition(worldposition.x + 300, worldposition.y + 30);
		mainSprite = this->getChildByName("TestBlock4");
		mainSprite->setPosition(cam->getPositionX(), cam->getPositionY());
	}
#endif // __ANDROID__

	if (isHeldDown)
	{
		if (touchtimer > 0)
		{
			touchtimer -= delta;
		}
		else
		{
			// display menu(TBA)
			touchtimer = ORIGINAL_TOUCH_TIMER;
		}
	}

	// buying and selling
	if (curGold >= CostNormal)
	{
		canBuyNormal = true;
	}
	else
	{
		canBuyNormal = false;
	}
	if (curGold >= CostMachine)
	{
		canBuyMachine = true;
	}
	else
	{
		canBuyMachine = false;
	}
	if (curGold >= CostSAM)
	{
		canBuySAM = true;
	}
	else
	{
		canBuySAM = false;
	}


	// player UI
	maxLivesLabel->setString(CCString::createWithFormat("Lives: %i", gm->instance()->GetMaxLives())->getCString());

	if (gm->instance()->GetMaxLives() <= 3)
		maxLivesLabel->setTextColor(Color4B::RED);
	else
		maxLivesLabel->setTextColor(Color4B::WHITE);

	waveNumLabel->setString(CCString::createWithFormat("Wave: %i", gm->instance()->GetWaveNum())->getCString());
	waveDelayLabel->setString(CCString::createWithFormat("%i", (int)gm->instance()->GetWaveSpawnDelay())->getCString());
	goldLabel->setString((CCString::createWithFormat("Gold: %i", curGold))->getCString());

	/*this->getChildByName("TowerSelectingBlock1")->setPosition(cam->getPositionX() * 0.125f, cam->getPositionY() * 0.125);*/

	// delay before the wave starts
	gm->instance()->SetWaveSpawnDelay(gm->instance()->GetWaveSpawnDelay() - delta);

	if (gm->instance()->GetWaveSpawnDelay() < 0.f)
	{
		waveDelayLabel->setVisible(false);

		timeCount += delta;
		if (timeCount >= timer)
		{
			if (gm->instance()->GetWaveNum() % 3 == 0)
				// boss wave
				spawnBoss();
			else
				// normal wave
				spawnEnemy();

			timeCount = 0;
		}
	}

	// check if x no. of bosses are killed
	if (gm->instance()->GetWaveNum() % 3 == 0)
	{
		if (gm->instance()->GetBossKilled() >= gm->instance()->GetBossMaxCount())
		{
			// check if there are no more enemies present
			if (gm->instance()->GetEnemyInGame() == 0)
			{
				curGold += 200;
				gm->instance()->SetNewWave();

				if (!objectManager.bulletVector.empty() || !objectManager.homingVector.empty())
				{
					/*for each (CBulletProjectile* projectile in objectManager.bulletVector)
					{
						bulletStorage.push_back(projectile);
					}*/
					for (int i = 0; i < objectManager.bulletVector.size(); i++)
					{
						bulletStorage.push_back(objectManager.bulletVector[i]);
					}
					for (int i = 0; i < objectManager.homingVector.size(); i++)
					{
						homingStorage.push_back(objectManager.homingVector[i]);
					}
				}

				// set visibility of waveDelayLabel back to true
				waveDelayLabel->setVisible(true);
			}
		}
	}
	else
	{
		if (gm->instance()->GetEnemyKilled() >= gm->instance()->GetEnemyMaxCount())
		{
			// check if there are no more enemies present
			if (gm->instance()->GetEnemyInGame() == 0)
			{
				curGold += 400;
				gm->instance()->SetNewWave();

				if (!objectManager.bulletVector.empty() || !objectManager.homingVector.empty())
				{
					/*for each (CBulletProjectile* projectile in objectManager.bulletVector)
					{
						bulletStorage.push_back(projectile);
					}*/
					for (int i = 0; i < objectManager.bulletVector.size(); i++)
					{
						bulletStorage.push_back(objectManager.bulletVector[i]);
					}
					for (int i = 0; i < objectManager.homingVector.size(); i++)
					{
						homingStorage.push_back(objectManager.homingVector[i]);
					}
				}

				// set visibility of waveDelayLabel back to true
				waveDelayLabel->setVisible(true);
			}
		}
	}



	// check for lives
	if (gm->instance()->GetMaxLives() < 0)
	{
		// stop whatever is going on
		//for each (CEnemy* enemy in objectManager.enemyVector)
		//{
		//	// remove all existing enemies
		//	enemy->SetDestroyed(true);
		//}

		for (int i = 0; i < objectManager.enemyVector.size(); i++)
		{
			objectManager.enemyVector[i]->SetDestroyed(true);
		}

		if (!objectManager.towerVector.empty())
		{
			/*for each (Tower* tower in objectManager.towerVector)
			{
				towerStorage.push_back(tower);
			}*/
			for (int i = 0; i < objectManager.towerVector.size(); i++)
			{
				towerStorage.push_back(objectManager.towerVector[i]);
			}
		}

		if (!objectManager.bulletVector.empty())
		{
			/*for each (CBulletProjectile* projectile in objectManager.bulletVector)
			{
				bulletStorage.push_back(projectile);
			}*/
			for (int i = 0; i < objectManager.bulletVector.size(); i++)
			{
				bulletStorage.push_back(objectManager.bulletVector[i]);
			}
		}

		if (!objectManager.homingVector.empty())
		{
			/*for each (CBulletProjectile* projectile in objectManager.bulletVector)
			{
				bulletStorage.push_back(projectile);
			}*/
			for (int i = 0; i < objectManager.homingVector.size(); i++)
			{
				homingStorage.push_back(objectManager.homingVector[i]);
			}
		}

		if (objectManager.enemyVector.empty() && objectManager.towerVector.empty())
		{
			// reset all 
			gm->instance()->ResetParameters();
			waveDelayLabel->setVisible(true);
		}

	}

	// tower tracking enemy
	for (int i = 0; i < objectManager.towerVector.size(); ++i)
	{
		for (int j = 0; j < objectManager.enemyVector.size(); ++j)
		{
			Tower* checkedTower = objectManager.towerVector.at(i);
			CEnemy* checkedEnemy = objectManager.enemyVector.at(j);
			if (checkedTower != nullptr && checkedTower->getCanShoot())
			{
				if (checkedEnemy != nullptr)
				{
					canTowerHit(checkedEnemy, checkedTower);
				}
			}
		}
	}

	for (int j = 0; j < objectManager.enemyVector.size(); ++j)
	{
		for (int i = 0; i < objectManager.bulletVector.size(); ++i)
		{
			CBulletProjectile* checkedBullet = objectManager.bulletVector.at(i);
			CEnemy* checkedEnemy = objectManager.enemyVector.at(j);


			if (checkedBullet != nullptr && checkedEnemy != nullptr)
			{
				if (checkedEnemy->type == CGameObject::GO_ENEMYGROUND) // remove projectile collision against enemy it's not supposed to hit
				{
					if (checkedBullet->target_type == CBulletProjectile::AIR)
					{
						continue;
					}
				}
				else if (checkedEnemy->type == CGameObject::GO_ENEMYFLYING)
				{
					if (checkedBullet->target_type == CBulletProjectile::GROUND)
					{
						continue;
					}
				}

				if ((checkedEnemy->getPositionX() - checkedBullet->getPositionX() <= 64 * 0.5f &&
					checkedEnemy->getPositionX() - checkedBullet->getPositionX() >= (64 * 0.5f)*-1) &&
					(checkedEnemy->getPositionY() - checkedBullet->getPositionY() <= 64 * 0.5f &&
						checkedEnemy->getPositionY() - checkedBullet->getPositionY() >= (64 * 0.5f)*-1))
				{
					checkedEnemy->DamageEnemy(checkedBullet->Damage);
					if (checkedEnemy->GetHealth() <= 0)
						checkedEnemy->SetDestroyed(true);
					if (checkedBullet->hasBeenRemoved == false)
					{
						bulletStorage.push_back(checkedBullet);
						checkedBullet->hasBeenRemoved = true;
					}
					audio->playEffect("blop.wav", false, 1.0f, 0.0f, 1.0f);
				}
				else if ((checkedBullet->getNumberOfRunningActions() == 0) && (checkedBullet->moveCheck == false) && (checkedBullet->hasBeenRemoved == false))
				{
					bulletStorage.push_back(checkedBullet);
					checkedBullet->hasBeenRemoved = true;
				}
			}
		}

		for (int Z = 0; Z < objectManager.homingVector.size(); ++Z)
		{
			CHomingProjectile* checkedHoming = objectManager.homingVector.at(Z);
			CEnemy* checkedEnemy = objectManager.enemyVector.at(j);


			if (checkedHoming != nullptr && checkedEnemy != nullptr)
			{
				if (checkedEnemy->type == CGameObject::GO_ENEMYGROUND) // remove projectile collision against enemy it's not supposed to hit
				{
					if (checkedHoming->target_type == CBulletProjectile::AIR)
					{
						continue;
					}
				}
				else if (checkedEnemy->type == CGameObject::GO_ENEMYFLYING)
				{
					if (checkedHoming->target_type == CBulletProjectile::GROUND)
					{
						continue;
					}
				}

				if ((checkedEnemy->getPositionX() - checkedHoming->getPositionX() <= 64 * 0.5f &&
					checkedEnemy->getPositionX() - checkedHoming->getPositionX() >= (64 * 0.5f)*-1) &&
					(checkedEnemy->getPositionY() - checkedHoming->getPositionY() <= 64 * 0.5f &&
						checkedEnemy->getPositionY() - checkedHoming->getPositionY() >= (64 * 0.5f)*-1))
				{
					checkedEnemy->DamageEnemy(checkedHoming->Damage);
					if (checkedEnemy->GetHealth() <= 0)
						checkedEnemy->SetDestroyed(true);
					audio->playEffect("blop.wav", false, 1.0f, 0.0f, 1.0f);
					if (checkedHoming->hasBeenRemoved == false)
					{
						checkedHoming->hasBeenRemoved = true;
						homingStorage.push_back(checkedHoming);
					}
				}
				else if (checkedHoming->locatedTarget->GetDestroyed())
				{
					if (checkedHoming->hasBeenRemoved == false)
					{
						checkedHoming->hasBeenRemoved = true;
						homingStorage.push_back(checkedHoming);
					}
				}
			}
		}
	}


	//each(CEnemy* enemy in objectManager.enemyVector)
	for (int i = 0; i < objectManager.enemyVector.size(); i++)
	{
		if (objectManager.enemyVector[i]->GetDestroyed())
			enemyStorage.push_back(objectManager.enemyVector[i]);
	}

	while (!enemyStorage.empty())
	{
		CEnemy* removedEnemy = enemyStorage.front();
		if (removedEnemy->GetHasReachedEnd())
		{
			gm->instance()->SetMaxLives(gm->instance()->GetMaxLives() - 1);
			audio->playEffect("bow sound.wav", false, 1.0f, 0.0f, 1.0f);
			if (removedEnemy->type == CGameObject::GO_ENEMYGROUND || removedEnemy->type == CGameObject::GO_ENEMYFLYING)
				gm->instance()->UpdateNumOfEnemiesKilled();
			else
				gm->instance()->UpdateNumOfBossKilled();
		}
		else
		{
			if (removedEnemy->type == CGameObject::GO_ENEMYGROUND || removedEnemy->type == CGameObject::GO_ENEMYFLYING)
				gm->instance()->UpdateNumOfEnemiesKilled();
			else
				gm->instance()->UpdateNumOfBossKilled();
			// gold reward to player
			curGold += removedEnemy->GetGoldReward();
		}

		enemyStorage.pop_front();
		switch (removedEnemy->type)
		{
		case CGameObject::GO_ENEMYFLYING:
			test->instance()->releaseFLyingEnemy((CFlyingEnemy*)removedEnemy);
			break;
		case CGameObject::GO_ENEMYGROUND:
			test->instance()->releaseGroundEnemy((CGroundEnemy*)removedEnemy);
			break;
		case CGameObject::GO_BOSSGROUND:
			test->instance()->releaseGroundBoss((CGroundBoss*)removedEnemy);
			break;
		case CGameObject::GO_BOSSFLYING:
			test->instance()->releaseFLyingBoss((CFlyingBoss*)removedEnemy);
			break;
		}

		this->removeChild(removedEnemy);
		objectManager.RemoveEnemy(removedEnemy);
	}

	while (!bulletStorage.empty())
	{
		CBulletProjectile* removedBullet = bulletStorage.front();
		bulletStorage.pop_front();
		test->instance()->releaseBullet(removedBullet);
		this->removeChild(removedBullet);
		objectManager.RemoveBullet(removedBullet);
	}

	while (!homingStorage.empty())
	{
		CHomingProjectile* removedHoming = homingStorage.front();
		homingStorage.pop_front();
		test->instance()->releaseHoming(removedHoming);
		this->removeChild(removedHoming);
		objectManager.RemoveHoming(removedHoming);
	}

	while (!towerStorage.empty())
	{
		Tower* removedTower = towerStorage.front();
		towerStorage.pop_front();

		test->instance()->releaseTower(removedTower);
		this->removeChild(removedTower);
		objectManager.RemoveTower(removedTower);
	}
}

void HelloWorld::spawnEnemy()
{
	if (gm->instance()->GetEnemyCount() != gm->instance()->GetEnemyMaxCount())
	{
		if (gm->instance()->GetEnemyCount() % 3 == 0)
		{
			CFlyingEnemy* gen = test->instance()->allocateFlyingEnemy();
			if (gen != NULL)
			{
				gen->init();
				gen->setAnchorPoint(Vec2(0, 0));
				gen->setName("FlyingEnemy");
				gen->setData(gen->getPosition());
				this->addChild(gen, 10);
				objectManager.AddNewEnemy(gen);
			}
		}
		else
		{
			CGroundEnemy* gen = test->instance()->allocateGroundEnemy();
			if (gen != NULL)
			{
				gen->init();
				gen->setAnchorPoint(Vec2(0, 0));
				gen->setName("GroundEnemy");
				gen->setData(gen->getPosition());
				this->addChild(gen, 10);
				objectManager.AddNewEnemy(gen);
			}
		}

		gm->instance()->UpdateNumOfEnemiesSpawned();
	}
}

void HelloWorld::spawnBoss()
{
	if (gm->instance()->GetBossCount() != gm->instance()->GetBossMaxCount())
	{
		// spawn flying boss every 6 waves
		if (gm->instance()->GetWaveNum() % 6 == 0)
		{
			auto gen = test->instance()->allocateFlyingBoss();
			if (gen != NULL)
			{
				gen->init();
				gen->setAnchorPoint(Vec2(0, 0));
				gen->setName("FlyingBoss");
				gen->setData(gen->getPosition());
				this->addChild(gen, 10);
				objectManager.AddNewEnemy(gen);
				gm->instance()->UpdateNumOfBossSpawned();
			}
		}
		// spawn ground boss every 3 waves
		else
		{
			auto gen = test->instance()->allocateGroundBoss();
			if (gen != NULL)
			{
				gen->init();
				gen->setAnchorPoint(Vec2(0, 0));
				gen->setName("GroundBoss");
				gen->setData(gen->getPosition());
				this->addChild(gen, 10);
				objectManager.AddNewEnemy(gen);
				gm->instance()->UpdateNumOfBossSpawned();
			}
		}
	}
}

bool HelloWorld::onMouseDown(Event* event)
{
	EventMouse* e = (EventMouse*)event;
	if (e->getLocationInView().y < 70)
	{
		if ((e->getLocationInView().x < FIRST_TOWER_POSITION + (spriteW*0.5f) && e->getLocationInView().x > FIRST_TOWER_POSITION - (spriteW*0.5f)) && canBuyNormal)
		{
			towerSelected = 0;
		}
		else if ((e->getLocationInView().x < (FIRST_TOWER_POSITION * 2) + (spriteW*0.5f) && e->getLocationInView().x >(FIRST_TOWER_POSITION * 2) - (spriteW*0.5f)) && canBuyMachine)
		{
			towerSelected = 1;
		}
		else if ((e->getLocationInView().x < (FIRST_TOWER_POSITION * 3) + (spriteW*0.5f) && e->getLocationInView().x >(FIRST_TOWER_POSITION * 3) - (spriteW*0.5f)) && canBuySAM)
		{
			towerSelected = 2;
		}
		else
		{
			towerSelected = -1;
			dragging = false;
			return false;
		}
		dragging = true;
		return true;
	}
	return false;
}
void HelloWorld::onMouseUp(Event* event)
{
	if (!dragging)
		return;

	EventMouse* e = (EventMouse*)event;
	Vec2 worldposition = Vec2(cam->getPositionX() - OriginalCameraPosition.x, cam->getPositionY() - OriginalCameraPosition.y);
	if (e->getLocationInView().y < 70)
	{
		switch (towerSelected)
		{
			case 0:
			{
				this->getChildByName("TestTower")->setPosition(worldposition.x + FIRST_TOWER_POSITION, worldposition.y + 30);
				this->getChildByName("RangeFinder")->setPosition(0, 0);
				this->getChildByName("RangeFinder")->setScale(0);
				break;
			}
			case 1:
			{
				this->getChildByName("TestTower2")->setPosition(worldposition.x + FIRST_TOWER_POSITION * 2, worldposition.y + 30);
				this->getChildByName("RangeFinder")->setPosition(0, 0);
				this->getChildByName("RangeFinder")->setScale(0);
				break;
			}
			case 2:
			{
				this->getChildByName("TestTower3")->setPosition(worldposition.x + FIRST_TOWER_POSITION * 3, worldposition.y + 30);
				this->getChildByName("RangeFinder")->setPosition(0, 0);
				this->getChildByName("RangeFinder")->setScale(0);
				break;
			}
			default:
			{
				break;
			}
		}
	}
	else
	{
		if (towerSelected == -1)
		{
			return;
		}

		Tower* gen = test->instance()->allocateTower();
		if (gen != NULL)
		{
			switch (towerSelected)
			{
				case 0:
				{
					this->getChildByName("TestTower")->setPosition(worldposition.x + FIRST_TOWER_POSITION, worldposition.y + 30);
					this->getChildByName("RangeFinder")->setPosition(0, 0);
					this->getChildByName("RangeFinder")->setScale(0);
					gen->init();
					curGold -= CostNormal;
					break;
				}
				case 1:
				{
					this->getChildByName("TestTower2")->setPosition(worldposition.x + FIRST_TOWER_POSITION * 2, worldposition.y + 30);
					this->getChildByName("RangeFinder")->setPosition(0, 0);
					this->getChildByName("RangeFinder")->setScale(0);
					gen->init(Tower::MACHINE_GUN);
					curGold -= CostMachine;
					break;
				}
				case 2:
				{
					this->getChildByName("TestTower3")->setPosition(worldposition.x + FIRST_TOWER_POSITION * 3, worldposition.y + 30);
					this->getChildByName("RangeFinder")->setPosition(0, 0);
					this->getChildByName("RangeFinder")->setScale(0);
					gen->init(Tower::SAM_SITE);
					curGold -= CostSAM;
					break;
				}
				default:
				{
					break;
				}
			}
			gen->setAnchorPoint(Vec2(0.5f, 0.5f));
			gen->setName("Tower");
			gen->setPosition(e->getLocationInView().x - (spriteW * 0.5f), e->getLocationInView().y - (spriteW * 0.5f));
			this->addChild(gen, 10);
			objectManager.AddNewTower(gen);
		}
	}
	dragging = false;
}
void HelloWorld::onMouseMove(Event* event)
{
	if (dragging)
	{
		EventMouse* e = (EventMouse*)event;
		Vec2 worldposition = e->getLocationInView();
		switch (towerSelected)
		{
		case 0:
		{
			this->getChildByName("TestTower")->setPosition(worldposition);
			this->getChildByName("RangeFinder")->setPosition(worldposition);
			this->getChildByName("RangeFinder")->setScale(RangeNormal);
			break;
		}
		case 1:
		{
			this->getChildByName("TestTower2")->setPosition(worldposition);
			this->getChildByName("RangeFinder")->setPosition(worldposition);
			this->getChildByName("RangeFinder")->setScale(RangeMachine);
			break;
		}
		case 2:
		{
			this->getChildByName("TestTower3")->setPosition(worldposition);
			this->getChildByName("RangeFinder")->setPosition(worldposition);
			this->getChildByName("RangeFinder")->setScale(RangeSAM);
			break;
		}
		default:
		{
			break;
		}
		}
	}
}

// Touch Events
bool HelloWorld::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{

	Vec2 worldposition = Vec2(cam->getPositionX() - OriginalCameraPosition.x, cam->getPositionY() - OriginalCameraPosition.y);
	// if position is not equal to the bottom of the map
	if (touch->getLocation().y < 250)
	{
		if ((touch->getLocation().x < FIRST_TOWER_POSITION + (spriteW) && touch->getLocation().x > FIRST_TOWER_POSITION - (spriteW)) && canBuyNormal)
		{
			towerSelected = 0;
		}
		else if ((touch->getLocation().x < (FIRST_TOWER_POSITION * 2) + (spriteW) && touch->getLocation().x >(FIRST_TOWER_POSITION * 2) - (spriteW)) && canBuyMachine)
		{
			towerSelected = 1;
		}
		else if ((touch->getLocation().x < (FIRST_TOWER_POSITION * 3) + (spriteW) && touch->getLocation().x >(FIRST_TOWER_POSITION * 3) - (spriteW)) && canBuySAM)
		{
			towerSelected = 2;
		}
		else
		{
			towerSelected = -1;
			return false;
		}
		dragging = true;
		return true;
	}
	return false;
}
void HelloWorld::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
	if (!dragging)
		return;

	Vec2 worldposition = Vec2(cam->getPositionX() - OriginalCameraPosition.x, cam->getPositionY() - OriginalCameraPosition.y);
	int offset;
#ifdef __ANDROID__
	offset = 150;
#elif __APPLE__
	offset = 100;
#endif //

    int versionButton;
#ifdef __ANDROID__
    versionButton = 30;
#elif __APPLE__
    versionButton = 80;
#endif //


    if (touch->getLocation().y < offset)
	{
		switch (towerSelected)
		{
		case 0:
		{
			this->getChildByName("TestTower")->setPosition(worldposition.x + FIRST_TOWER_POSITION, worldposition.y + versionButton);
			this->getChildByName("RangeFinder")->setPosition(0, 0);
			this->getChildByName("RangeFinder")->setScale(0);
			break;
		}
		case 1:
		{
			this->getChildByName("TestTower2")->setPosition(worldposition.x + FIRST_TOWER_POSITION * 2, worldposition.y + versionButton);
			this->getChildByName("RangeFinder")->setPosition(0, 0);
			this->getChildByName("RangeFinder")->setScale(0);
			break;
		}
		case 2:
		{
			this->getChildByName("TestTower3")->setPosition(worldposition.x + FIRST_TOWER_POSITION * 3, worldposition.y + versionButton);
			this->getChildByName("RangeFinder")->setPosition(0, 0);
			this->getChildByName("RangeFinder")->setScale(0);
			break;
		}
		default:
		{
			break;
		}
		}
	}
	else
	{
		if (towerSelected == -1)
		{
			return;
		}
		Tower* gen = test->instance()->allocateTower();
		if (gen != NULL)
		{
			switch (towerSelected)
			{
			case 0:
			{
				//check tower cost then plant tower
				this->getChildByName("TestTower")->setPosition(worldposition.x + FIRST_TOWER_POSITION, worldposition.y + versionButton);
				this->getChildByName("RangeFinder")->setPosition(0, 0);
				this->getChildByName("RangeFinder")->setScale(0);
				curGold -= CostNormal;
				gen->init();
				break;
			}
			case 1:
			{
				this->getChildByName("TestTower2")->setPosition(worldposition.x + FIRST_TOWER_POSITION * 2, worldposition.y + versionButton);
				this->getChildByName("RangeFinder")->setPosition(0, 0);
				this->getChildByName("RangeFinder")->setScale(0);
				curGold -= CostMachine;
				gen->init(Tower::MACHINE_GUN);
				break;
			}
			case 2:
			{
				this->getChildByName("TestTower3")->setPosition(worldposition.x + FIRST_TOWER_POSITION * 3, worldposition.y + versionButton);
				this->getChildByName("RangeFinder")->setPosition(0, 0);
				this->getChildByName("RangeFinder")->setScale(0);
				curGold -= CostSAM;
				gen->init(Tower::SAM_SITE);
				break;
			}
			default:
			{
				break;
			}
			}

			int offset2;
#ifdef __ANDROID__
			offset2 = 150;
#elif __APPLE__
			offset2 = 80;
#endif // 

			gen->setAnchorPoint(Vec2(0.5f, 0.5f));
			gen->setName("Tower");
			gen->setPosition(touch->getLocation().x - (spriteW * 0.5f), touch->getLocation().y - (spriteW * 0.5f) - offset2);
			this->addChild(gen, 10);
			objectManager.AddNewTower(gen);
		}
	}
	dragging = false;
}
void HelloWorld::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
	if (dragging)
	{
		Vec2 worldposition = touch->getLocation();
		int offset3;
#ifdef __ANDROID__
		offset3 = 150;
#elif __APPLE__
		offset3 = 80;
#endif // 
		worldposition.y -= offset3;
		switch (towerSelected)
		{
		case 0:
		{
			this->getChildByName("TestTower")->setPosition(worldposition);
			this->getChildByName("RangeFinder")->setPosition(worldposition);
			this->getChildByName("RangeFinder")->setScale(RangeNormal);
			break;
		}
		case 1:
		{
			this->getChildByName("TestTower2")->setPosition(worldposition);
			this->getChildByName("RangeFinder")->setPosition(worldposition);
			this->getChildByName("RangeFinder")->setScale(RangeMachine);
			break;
		}
		case 2:
		{
			this->getChildByName("TestTower3")->setPosition(worldposition);
			this->getChildByName("RangeFinder")->setPosition(worldposition);
			this->getChildByName("RangeFinder")->setScale(RangeSAM);
			break;
		}
		default:
		{
			break;
		}
		}
	}
}
//void

bool HelloWorld::canTowerHit(CEnemy* _type, Tower* _tower)
{
	if ((_type->type == CGameObject::GO_ENEMYGROUND || _type->type == CGameObject::GO_BOSSGROUND) && _tower->getAttackType(Tower::CAN_ATTACK_GROUND))
	{
		if ((_type->getPositionX() - _tower->getPositionX() <= _tower->getRange() &&
			_type->getPositionX() - _tower->getPositionX() >= _tower->getRange() * -1) &&
			(_type->getPositionY() - _tower->getPositionY() <= _tower->getRange() &&
				_type->getPositionY() - _tower->getPositionY() >= _tower->getRange() * -1))
		{
			//Monsters[j]->DamageEnemy(10);
			CBulletProjectile* gen = test->instance()->allocateBullet();
			gen->init();
			gen->setAnchorPoint(Vec2(0, 0));
			gen->setName("Bullet");
			if (_tower->getTowerType() == Tower::NORMAL)
			{
				gen->setData(_tower->getPosition(), _type->getPosition(), 0.50f);
				gen->Damage = 5;
			}
			else if (_tower->getTowerType() == Tower::MACHINE_GUN)
			{
				gen->setData(_tower->getPosition(), _type->getPosition(), 0.25f);
				gen->Damage = 3;
			}
			gen->target_type = CProjectileBase::GROUND;
			if (_tower->getAttackType(Tower::CAN_ATTACK_AIR))
			{
				gen->target_type = CProjectileBase::BOTH;
			}
			this->addChild(gen, 10);
			objectManager.AddNewBullet(gen);
			_tower->goOnCooldown();
		}
	}
	else if ((_type->type == CGameObject::GO_ENEMYFLYING || _type->type == CGameObject::GO_BOSSFLYING) && _tower->getAttackType(Tower::CAN_ATTACK_AIR))
	{
		if ((_type->getPositionX() - _tower->getPositionX() <= _tower->getRange() &&
			_type->getPositionX() - _tower->getPositionX() >= _tower->getRange() * -1) &&
			(_type->getPositionY() - _tower->getPositionY() <= _tower->getRange() &&
				_type->getPositionY() - _tower->getPositionY() >= _tower->getRange()  * -1))
		{
			//Monsters[j]->DamageEnemy(10);
			if (_tower->getTowerType() == Tower::SAM_SITE)
			{
				CHomingProjectile* gen = test->instance()->allocateHoming();
				gen->init();
				gen->setAnchorPoint(Vec2(0, 0));
				gen->setName("Homing");
				gen->setData(_tower->getPosition(), _type, 0.25f);
				gen->Damage = 8;
				gen->target_type = CProjectileBase::AIR;
				this->addChild(gen, 10);
				objectManager.AddNewHoming(gen);
			}
			else
			{
				CBulletProjectile* gen = test->instance()->allocateBullet();
				gen->init();
				gen->setAnchorPoint(Vec2(0, 0));
				gen->setName("Bullet");
				gen->setData(_tower->getPosition(), _type->getPosition(), 0.25f);
				gen->Damage = 3;
				gen->target_type = CProjectileBase::BOTH;
				this->addChild(gen, 10);
				objectManager.AddNewBullet(gen);
			}

			_tower->goOnCooldown();
		}
	}
	return false;
}
