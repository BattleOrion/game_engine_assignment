#ifndef OBJECT_MANAGER_H
#define OBJECT_MANAGER_H

#include "cocos2d.h"
#include "GameObject.h"
#include "Enemy.h"
#include "Projectile.h"
#include "Tower.h"

using namespace std;
using namespace cocos2d;

class CObjectManager
{
private:



public:
	vector<CEnemy*> enemyVector;
	vector<CBulletProjectile*> bulletVector;
	vector<CHomingProjectile*> homingVector;
	vector<Tower*> towerVector;


	CObjectManager();
	~CObjectManager();

	void AddNewEnemy(CEnemy* newEnemy);
	void AddNewBullet(CBulletProjectile* newBullet);
	void AddNewHoming(CHomingProjectile* newHoming);
	void AddNewTower(Tower* newTower);

	void RemoveEnemy(CEnemy* removedEnemy);
	void RemoveBullet(CBulletProjectile* removedBullet);
	void RemoveHoming(CHomingProjectile* removedHoming);
	void RemoveTower(Tower* removedTower);

	int getMaxSize();
};


#endif // !OBJECT_MANAGER_H