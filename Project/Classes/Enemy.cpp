#include "Enemy.h"
#include "ObjectPool.h"

using namespace cocos2d;

CEnemy::CEnemy()
	: health(1)
	, initialHealth(1)
	, maxHealth(1)
	, speed(1)
	, goldReward(0)
	, waypointID(0)
	, waypointNum()
	, destroyed(false)
	, hasReachedEnd(false)
	, gm(NULL)
	/*, animFrames(NULL)
	, animation(NULL)
	, spritebatch(NULL)
	, cache(NULL)*/
{
	//allocator::AllocatorStrategyPool<CEnemy> _allocator("EnemyAllocator", 50);

}

CEnemy::~CEnemy()
{
	sprite->release();
	//gm->instance()->~CGameManager();
}

bool CEnemy::init()
{
	health = 10;
	speed = 1;
	goldReward = 1;
	waypointID = 0;
	destroyed = false;
	hasReachedEnd = false;
	sprite = Sprite::create("Blue_Front1.png");
	sprite->retain();

	hpLabel = Label::createWithTTF("", "fonts/Marker Felt.ttf", 24);
	hpLabel->retain();
	if (hpLabel != nullptr)
	{
		hpLabel->setPosition(Vec2(0, 3));
	}
	this->addChild(hpLabel, 1);

	cWaypointManager.Init();
	
	auto visibleSize = Director::getInstance()->getVisibleSize();
	/*waypointID = cWaypointManager.AddWaypoint(Vec2(visibleSize.width / 2, 0));
	waypointID = cWaypointManager.AddWaypoint(waypointID, Vec2(0, visibleSize.height / 2));
	waypointID = cWaypointManager.AddWaypoint(waypointID, Vec2(visibleSize.width, visibleSize.height / 2));*/

	waypointNum[0] = cWaypointManager.AddWaypoint(Vec2(visibleSize.width / 2, 0));
	waypointNum[1] = cWaypointManager.AddWaypoint(waypointNum[0], Vec2(0, visibleSize.height / 2));
	waypointNum[2] = cWaypointManager.AddWaypoint(waypointNum[1], Vec2(visibleSize.width, visibleSize.height / 2));

	CWaypoint* waypoint = cWaypointManager.GetWaypoint(waypointNum[0]);
	auto moveEvent = MoveTo::create(5.0f, waypoint->GetPosition());
	this->runAction(moveEvent);

	this->scheduleUpdate();
	return true;
}

CEnemy * CEnemy::create(const std::string & file)
{
	CEnemy* enemy = new CEnemy();
	if (enemy)
	{
		enemy->autorelease();
		//enemy->sprite = Sprite::create(file);
		enemy->init();

		return enemy;
	}

	CC_SAFE_DELETE(enemy);
	return NULL;
}

CEnemy * CEnemy::initWithEnemy(CEnemy * _enemy)
{
	return NULL;
}

int CEnemy::GetWaypointID()
{
	return waypointID;
}

void CEnemy::SetWaypointID(int _waypointID)
{
	waypointID = _waypointID;
}

void CEnemy::DamageEnemy(int _dmg)
{
	if (health >= 0)
	{
		health -= _dmg;
		Sequence* action = Sequence::create(Blink::create(0.5f, 2), Show::create(), NULL);
		runAction(action);
	}
		
	else
		return;
}

void CEnemy::SetWaypoints()
{
	//visibleSize = Director::getInstance()->getVisibleSize();
	//waypointNum[0] = cWaypointManager.AddWaypoint(Vec2(visibleSize.width / 2, 0));
	//waypointNum[1] = cWaypointManager.AddWaypoint(/*waypointNum[0],*/ Vec2(0, visibleSize.height / 2));
	//waypointNum[2] = cWaypointManager.AddWaypoint(/*waypointNum[1],*/ Vec2(visibleSize.width, visibleSize.height / 2));
#ifdef __ANDROID__
    waypointNum[0] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 80,
                                                           visibleSize.height * 0.5f - 60));
    waypointNum[1] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 80,
                                                           visibleSize.height * 0.5f - 200));
    waypointNum[2] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 240,
                                                           visibleSize.height * 0.5f - 200));
    waypointNum[3] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 240,
                                                           visibleSize.height - 80));
    waypointNum[4] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 80,
                                                           visibleSize.height - 80));
    waypointNum[5] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 80,
                                                           visibleSize.height * 0.5f + 60));
    waypointNum[6] = cWaypointManager.AddWaypoint(Vec2(    40,
                                                           visibleSize.height * 0.5f + 60));
    waypointNum[7] = cWaypointManager.AddWaypoint(Vec2(    40,
                                                           visibleSize.height - 80));
    waypointNum[8] = cWaypointManager.AddWaypoint(Vec2(    200,
                                                           visibleSize.height - 80));
    waypointNum[9] = cWaypointManager.AddWaypoint(Vec2(    200,
                                                           visibleSize.height * 0.5f - 200));
    waypointNum[10] = cWaypointManager.AddWaypoint(Vec2( 0,
                                                         visibleSize.height * 0.5f - 200));
#elif __APPLE__
    waypointNum[0] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 100,
                                                       visibleSize.height * 0.5f - 20));
    waypointNum[1] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 100,
                                                       visibleSize.height * 0.5f - 170));
    waypointNum[2] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 280,
                                                       visibleSize.height * 0.5f - 170));
    waypointNum[3] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 280,
                                                       visibleSize.height - 20));
    waypointNum[4] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 100,
                                                       visibleSize.height - 20));
    waypointNum[5] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 100,
                                                       visibleSize.height * 0.5f + 140));
    waypointNum[6] = cWaypointManager.AddWaypoint(Vec2(    50,
                                                       visibleSize.height * 0.5f + 140));
    waypointNum[7] = cWaypointManager.AddWaypoint(Vec2(    50,
                                                       visibleSize.height - 20));
    waypointNum[8] = cWaypointManager.AddWaypoint(Vec2(    200,
                                                       visibleSize.height - 20));
    waypointNum[9] = cWaypointManager.AddWaypoint(Vec2(    200,
                                                       visibleSize.height * 0.5f - 170));
    waypointNum[10] = cWaypointManager.AddWaypoint(Vec2( 0,
                                                        visibleSize.height * 0.5f - 170));
#else
    waypointNum[0] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 100,
                                                       visibleSize.height * 0.5f - 150));
    waypointNum[1] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 100,
                                                       visibleSize.height * 0.5f - 350));
    waypointNum[2] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 280,
                                                       visibleSize.height * 0.5f - 350));
    waypointNum[3] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 280,
                                                       visibleSize.height - 150));
    waypointNum[4] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 100,
                                                       visibleSize.height - 150));
    waypointNum[5] = cWaypointManager.AddWaypoint(Vec2(    visibleSize.width - 100,
                                                       visibleSize.height - 300));
    waypointNum[6] = cWaypointManager.AddWaypoint(Vec2(    50,
                                                       visibleSize.height - 300));
    waypointNum[7] = cWaypointManager.AddWaypoint(Vec2(    50,
                                                       visibleSize.height - 150));
    waypointNum[8] = cWaypointManager.AddWaypoint(Vec2(    200,
                                                       visibleSize.height - 150));
    waypointNum[9] = cWaypointManager.AddWaypoint(Vec2(    200,
                                                       visibleSize.height * 0.5f - 350));
    waypointNum[10] = cWaypointManager.AddWaypoint(Vec2( 0,
                                                        visibleSize.height * 0.5f - 350));
#endif
	
}

void CEnemy::setData(Vec2 spawnLoc)
{
	setPosition(spawnLoc);
}

int CEnemy::GetHealth()
{
	return health;
}

int CEnemy::GetGoldReward()
{
	return goldReward;
}

int CEnemy::SetHealth(int waveNo)
{
	return initialHealth * waveNo;
}

void CEnemy::SetDestroyed(bool _value)
{
	destroyed = _value;
}

bool CEnemy::GetDestroyed()
{
	return destroyed;
}

void CEnemy::SetHasReachedEnd(bool _value)
{
	hasReachedEnd = _value;
}

bool CEnemy::GetHasReachedEnd()
{
	return hasReachedEnd;
}

void CEnemy::GoToNextWaypoint()
{	
	float distance = cWaypointManager.GetNextWaypointPosition().distance(this->getPosition());
	waypointID++;
	if (waypointID < 11)
	{
		CWaypoint* waypoint = cWaypointManager.GetWaypoint(waypointNum[waypointID]);
		Vec2 newPos = waypoint->GetPosition();

		float time = distance / this->speed;
		this->stopAllActions();
		auto moveEvent = MoveTo::create(time, newPos);
		
		this->runAction(moveEvent);		
	}
	
	else
	{
		this->stopAllActions();
		this->hasReachedEnd = true;
		this->destroyed = true;
	}
}

void CEnemy::update(float delta)
{
	if (cWaypointManager.HasReachedWaypoint(this->getPosition()))
		GoToNextWaypoint();	

	if (hpBar)
		hpBar->setScaleX(MathUtil::lerp(hpBar->getScaleX(), (float)health / maxHealth, 0.1f));
}

void CEnemy::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
	sprite->draw(renderer, transform, flags);
}

CGroundEnemy::CGroundEnemy()
{
}

CGroundEnemy::~CGroundEnemy()
{
}

bool CGroundEnemy::init()
{
	gm = CGameManager::instance();
	visibleSize = Director::getInstance()->getVisibleSize();

	//CEnemy::init();
	initialHealth = 10;
	health = SetHealth(gm->GetWaveNum());
	maxHealth = health;
	speed = 100;
	goldReward = 75;
	waypointID = 0;
	destroyed = false;
	hasReachedEnd = false;
	type = GO_ENEMYGROUND;

#ifdef __ANDROID__
    setPosition(0, visibleSize.height * 0.5f - 60);
#elif __APPLE__
    setPosition(0, visibleSize.height * 0.5f - 20);
#else
    setPosition(0, visibleSize.height * 0.5f - 150);
#endif

	this->removeAllChildren();

	hpBar->setPosition(Vec2(sprite->getBoundingBox().size.width * 0.5, sprite->getBoundingBox().size.height + 10));
	hpBar->setScaleY(hpBar->getScaleY() * 0.1f);
	this->addChild(hpBar, 1);
	
	//auto visibleSize = Director::getInstance()->getVisibleSize();
	//waypointNum[0] = cWaypointManager.AddWaypoint(Vec2(visibleSize.width / 2, 0));
	//waypointNum[1] = cWaypointManager.AddWaypoint(/*waypointNum[0],*/ Vec2(0, visibleSize.height / 2));
	//waypointNum[2] = cWaypointManager.AddWaypoint(/*waypointNum[1],*/ Vec2(visibleSize.width, visibleSize.height / 2));
	SetWaypoints();

	CWaypoint* waypoint = cWaypointManager.GetWaypoint(waypointNum[0]);
	float distance = waypoint->GetPosition().distance(this->getPosition());
	float time = distance / this->speed;
	auto moveEvent = MoveTo::create(time, waypoint->GetPosition());
	this->runAction(moveEvent);

	this->scheduleUpdate();
	return true;
}

CFlyingEnemy::CFlyingEnemy()
{
}

CFlyingEnemy::~CFlyingEnemy()
{
}

bool CFlyingEnemy::init()
{
	gm = CGameManager::instance();
	visibleSize = Director::getInstance()->getVisibleSize();

	//CEnemy::init();
	initialHealth = 7;
	health = SetHealth(gm->GetWaveNum());
	maxHealth = health;
	speed = 150;
	goldReward = 50;
	waypointID = 0;
	destroyed = false;
	hasReachedEnd = false;
	type = GO_ENEMYFLYING;

#ifdef __ANDROID__
    setPosition(0, visibleSize.height * 0.5f - 60);
#elif __APPLE__
    setPosition(0, visibleSize.height * 0.5f - 20);
#else
    setPosition(0, visibleSize.height * 0.5f - 150);
#endif
	

	this->removeAllChildren();
	
	hpBar->setPosition(Vec2(sprite->getBoundingBox().size.width * 0.5,
		sprite->getBoundingBox().size.height + 10));

	hpBar->setScaleY(hpBar->getScaleY() * 0.1f);
	this->addChild(hpBar, 1);

	//auto visibleSize = Director::getInstance()->getVisibleSize();
	//waypointNum[0] = cWaypointManager.AddWaypoint(Vec2(visibleSize.width / 2, 0));
	//waypointNum[1] = cWaypointManager.AddWaypoint(/*waypointNum[0],*/ Vec2(0, visibleSize.height / 2));
	//waypointNum[2] = cWaypointManager.AddWaypoint(/*waypointNum[1],*/ Vec2(visibleSize.width, visibleSize.height / 2));
	SetWaypoints();

	CWaypoint* waypoint = cWaypointManager.GetWaypoint(waypointNum[0]);
	float distance = waypoint->GetPosition().distance(this->getPosition());
	float time = distance / this->speed;
	auto moveEvent = MoveTo::create(time, waypoint->GetPosition());
	this->runAction(moveEvent);
	
	this->scheduleUpdate();
	return true;
}

CGroundBoss::CGroundBoss()
{
}

CGroundBoss::~CGroundBoss()
{
}

bool CGroundBoss::init()
{
	gm = CGameManager::instance();
	visibleSize = Director::getInstance()->getVisibleSize();

	//CEnemy::init();
	initialHealth = 50;
	health = SetHealth(gm->GetWaveNum());
	maxHealth = health;
	speed = 75;
	goldReward = 150;
	waypointID = 0;
	destroyed = false;
	hasReachedEnd = false;
	type = GO_BOSSGROUND;

#ifdef __ANDROID__
    setPosition(0, visibleSize.height * 0.5f - 60);
#endif
#ifdef __APPLE__
    setPosition(0, visibleSize.height * 0.5f - 20);
#endif
#ifdef _WINDOWS_
    setPosition(0, visibleSize.height * 0.5f - 150);
#endif

	this->removeAllChildren();

	hpBar->setPosition(Vec2(sprite->getBoundingBox().size.width * 0.5,
		sprite->getBoundingBox().size.height + 10));

	hpBar->setScaleY(hpBar->getScaleY() * 0.3f);
	this->addChild(hpBar, 1);

	SetWaypoints();

	CWaypoint* waypoint = cWaypointManager.GetWaypoint(waypointNum[0]);
	float distance = waypoint->GetPosition().distance(this->getPosition());
	float time = distance / this->speed;
	auto moveEvent = MoveTo::create(time, waypoint->GetPosition());
	this->runAction(moveEvent);

	this->scheduleUpdate();
	return true;
}

CFlyingBoss::CFlyingBoss()
{
}

CFlyingBoss::~CFlyingBoss()
{
}

bool CFlyingBoss::init()
{
	gm = CGameManager::instance();
	visibleSize = Director::getInstance()->getVisibleSize();

	//CEnemy::init();
	initialHealth = 35;
	health = SetHealth(gm->GetWaveNum());
	maxHealth = health;
	speed = 75;
	goldReward = 125;
	waypointID = 0;
	destroyed = false;
	hasReachedEnd = false;
	type = GO_BOSSFLYING;

#ifdef __ANDROID__
    setPosition(0, visibleSize.height * 0.5f - 40);
#endif
#ifdef __APPLE__
	setPosition(0, visibleSize.height * 0.5f - 20);
#endif
#ifdef _WINDOWS_
	setPosition(0, visibleSize.height * 0.5f - 150);
#endif

	this->removeAllChildren();

	hpBar->setPosition(Vec2(sprite->getBoundingBox().size.width * 0.5,
		sprite->getBoundingBox().size.height + 10));

	hpBar->setScaleY(hpBar->getScaleY() * 0.3f);
	this->addChild(hpBar, 1);

	SetWaypoints();

	CWaypoint* waypoint = cWaypointManager.GetWaypoint(waypointNum[0]);
	float distance = waypoint->GetPosition().distance(this->getPosition());
	float time = distance / this->speed;
	auto moveEvent = MoveTo::create(time, waypoint->GetPosition());
	this->runAction(moveEvent);

	this->scheduleUpdate();
	return true;
}
