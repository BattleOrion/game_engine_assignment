#ifndef _WAYPOINTMANAGER_H_
#define _WAYPOINTMANAGER_H_

#include "cocos2d.h"
#include "Waypoint.h"
#include <vector>
using namespace cocos2d;
using namespace std;

class CWaypointManager
{
public:
	// Constructor
	CWaypointManager();
	// Destructor
	virtual ~CWaypointManager();

	// Init
	void Init(void);
	// Destroy the instance
	void Destroy(void);

	// Add waypoint
	int AddWaypoint(Vec2 position);
	// Add the position of a waypoint by searching for its m_iID
	int AddWaypoint(const int m_iID, Vec2 position);
	// Remove waypoint based on its m_iID
	bool RemoveWaypoint(const int m_iID);

	// Get next ID when adding a waypoint
	int GetNextIDToAdd(void);
	// Get current ID
	int GetCurrentID(void);
	// Get Next Waypoint ID
	int GetNextWaypointID(void);

	// HasReachedWaypoint
	bool HasReachedWaypoint(Vec2 aPosition);
	// Get a Waypoint based on its ID
	CWaypoint* GetWaypoint(const int m_iID);
	// Get nearest waypoint amongst related Waypoints
	// This method is used when your NPC has deviated from the Waypoints and it needs to find the nearest Waypoint to go to
	CWaypoint* GetNearestWaypoint(Vec2 aPosition);
	// Get next Waypoint
	CWaypoint* GetNextWaypoint(void);
	// Get next Waypoint position
	Vec2 GetNextWaypointPosition(void);
	// Get the number of related Waypoints
	int GetNumberOfWaypoints(void)const;

	// PrintSelf
	void PrintSelf(void);

protected:
	// The current Waypoint's ID
	int m_iCurrentID;
	// Distance tolerance for checking proximity to a Waypoint
	float m_fDistanceTolerance;

	vector<CWaypoint*> listOfWaypoints;

	// Remove related waypoint
	void RemoveRelatedWaypoint(const int m_iID);
};

#endif
