#include "GameManager.h"

CGameManager* CGameManager::s_instance = NULL;

CGameManager* CGameManager::instance()
{
	if (!s_instance)
		s_instance = new CGameManager();
	return s_instance;
}

CGameManager::CGameManager()
	: maxLives(5)
	, waveNum(0)
	, waveSpawnDelay(4.0f)
	, enemyCount(0)
	, enemyMaxCount(0)
	, enemyKilled(0)
	, bossCount(0)
	, bossMaxCount(0)
	, bossKilled(0)
	, enemyInGame(0)
	, canSpawnEnemy(true)
{
}

CGameManager::~CGameManager()
{
}

int CGameManager::GetMaxLives()
{
	return maxLives;
}

void CGameManager::SetMaxLives(int value)
{
	maxLives = value;
}

int CGameManager::GetWaveNum()
{
	return waveNum;
}

void CGameManager::SetWaveNum(int value)
{
	waveNum = value;
}

float CGameManager::GetWaveSpawnDelay()
{
	return waveSpawnDelay;
}

void CGameManager::SetWaveSpawnDelay(float value)
{
	waveSpawnDelay = value;
}

int CGameManager::GetEnemyInGame()
{
	return enemyInGame;
}

void CGameManager::SetEnemyInGame(int value)
{
	enemyInGame = value;
}

int CGameManager::GetEnemyCount()
{
	return enemyCount;
}

void CGameManager::SetEnemyCount(int value)
{
	enemyCount = value;
}

int CGameManager::GetEnemyMaxCount()
{
	return enemyMaxCount;
}

void CGameManager::SetEnemyMaxCount(int value)
{
	enemyMaxCount = value;
}

int CGameManager::GetEnemyKilled()
{
	return enemyKilled;
}

void CGameManager::SetEnemyKilled(int value)
{
	enemyKilled = value;
}

int CGameManager::GetBossCount()
{
	return bossCount;
}

void CGameManager::SetBossCount(int value)
{
	bossCount = value;
}

int CGameManager::GetBossMaxCount()
{
	return bossMaxCount;
}

void CGameManager::SetBossMaxCount(int value)
{
	bossMaxCount = value;
}

int CGameManager::GetBossKilled()
{
	return bossKilled;
}

void CGameManager::SetBossKilled(int value)
{
	bossKilled = value;
}

bool CGameManager::GetCanSpawnEnemy()
{
	return canSpawnEnemy;
}

void CGameManager::SetCanSpawnEnemy(bool value)
{
	canSpawnEnemy = value;
}

void CGameManager::SetParameters()
{
	maxLives = 10;
	waveNum = 1;
	enemyCount = 0;
	enemyMaxCount = 5;
	enemyKilled = 0;
	bossCount = 0;
	bossMaxCount = 1;
	bossKilled = 0;
	enemyInGame = 0;
	canSpawnEnemy = true;
}

void CGameManager::ResetParameters()
{
	maxLives = 10;
	waveNum = 1;
	enemyCount = 0;
	enemyMaxCount = 5;
	enemyKilled = 0;
	bossCount = 0;
	bossMaxCount = 1;
	bossKilled = 0;
	enemyInGame = 0;
	canSpawnEnemy = true;

	waveSpawnDelay = 4;
}

void CGameManager::SetNewWave()
{
	++waveNum;
	waveSpawnDelay = 4.f;
	enemyKilled = 0;
	enemyCount = 0;
	enemyInGame = 0;
	enemyMaxCount += 10;
}

void CGameManager::UpdateNumOfEnemiesKilled()
{
	++enemyKilled;
	--enemyInGame;
}

void CGameManager::UpdateNumOfBossKilled()
{
	++bossKilled;
	--enemyInGame;
}
void CGameManager::UpdateNumOfEnemiesSpawned()
{
	++enemyInGame;
	++enemyCount;
}

void CGameManager::UpdateNumOfBossSpawned()
{
	++enemyInGame;
	++bossCount;
}
