#include "Projectile.h"

CProjectileBase::CProjectileBase()
{
}

CProjectileBase::~CProjectileBase()
{
}

void CProjectileBase::update(float _dt)
{
	// Does nothing here, can inherit & override or create your own version of this class :D
}

CBulletProjectile::CBulletProjectile()
{
	//this->scheduleUpdate();
}

CBulletProjectile::~CBulletProjectile()
{
}

bool CBulletProjectile::init()
{
	moveCheck = true;
	hasBeenRemoved = false;
	SpeedBuffer = 1.0f;
	Damage = -1;
	this->scheduleUpdate();
	return true;
}

void CBulletProjectile::update(float _dt)
{
	if (moveCheck)
	{
		this->stopAllActions();
		auto moveEvent = MoveTo::create(timer * SpeedBuffer, Vec2(targetLocation.x, targetLocation.y));
		Vec2 angle = targetLocation - getPosition();
		float angleRadians = atan2f((float)angle.y, (float)angle.x);
		float angleDegrees = CC_RADIANS_TO_DEGREES(angleRadians);
		float cocosAngle = -1 * angleDegrees;
		if (angleRadians < 0) {
			cocosAngle += 360;
		}
		this->setRotation((float)cocosAngle);
		this->runAction(moveEvent);
		moveCheck = false;
	}
}

void CBulletProjectile::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
	sprite->draw(renderer, transform, flags);
}

void CBulletProjectile::setData(Vec2 spawnLoc, Vec2 enemyLoc, float _speedbuffer)
{
	setPosition(spawnLoc);
	SetTarget(enemyLoc);
	setTimer(spawnLoc.distance(enemyLoc) / 100);
	SpeedBuffer = _speedbuffer;
}

void CBulletProjectile::SetTarget(Vec2 setTarget)
{
	targetLocation = setTarget;
}

void CBulletProjectile::setTimer(float time)
{
	timer = time;
}

bool CBulletProjectile::GetMoveCheck()
{
	return moveCheck;
}

CHomingProjectile::CHomingProjectile()
{
	//this->scheduleUpdate();
}

CHomingProjectile::~CHomingProjectile()
{
}

bool CHomingProjectile::init()
{
	moveMode = true;
	hasBeenRemoved = false;
	this->scheduleUpdate();
	return true;
}

void CHomingProjectile::update(float _dt)
{
	if (locatedTarget)
	{
		if (moveMode)
		{
			this->stopAllActions();
			auto moveEvent = MoveTo::create((getPosition().distance(locatedTarget->getPosition()) / 100) * SpeedBuffer, locatedTarget->getPosition());
			Vec2 angle = locatedTarget->getPosition() - getPosition();
			float angleRadians = atan2f((float)angle.y, (float)angle.x);
			float angleDegrees = CC_RADIANS_TO_DEGREES(angleRadians);
			float cocosAngle = -1 * angleDegrees;
			if (angleRadians < 0) {
				cocosAngle += 360;
			}
			this->setRotation((float)cocosAngle);
			this->runAction(moveEvent);
			moveMode = false;
		}
		else
		{
			moveMode = true;
		}
	}
}

void CHomingProjectile::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
	sprite->draw(renderer, transform, flags);
}

void CHomingProjectile::setData(Vec2 spawnLoc, CEnemy* enemySet, float _speedbuffer)
{
	setPosition(spawnLoc);
	SetTarget(enemySet);
	SpeedBuffer = _speedbuffer;
}

void CHomingProjectile::SetTarget(CEnemy* enemyTarget)
{
	locatedTarget = enemyTarget;
}

