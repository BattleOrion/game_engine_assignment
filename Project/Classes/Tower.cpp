#include "Tower.h"

#define TOWER_RANGE_MULTIPLIER 50

Tower::Tower() : target(nullptr)
{
}

Tower::~Tower()
{
	delete target;
	target = nullptr;
}

bool Tower::init(TowerType type)
{
	for (int i = 0; i < TOWER_ATTACK_TYPE_TOTAL; ++i)
	{
		AttackType[i] = NONE;
	}
	towerType = type;
	Vec2 temp = Vec2(0.5f, 0.5f);
	switch (type)
	{
	case Tower::NORMAL:
		Range = 4;
		TowerCost = 250;
		Damage = 2;
		RateOfFire = 0.75f;
		turnAngle = -1;
		AttackType[CAN_ATTACK_GROUND] = CAN_ATTACK_GROUND;
		sprite = Sprite::create("towerDefense_tile134.png");
		sprite->setAnchorPoint(temp);
		sprite->retain();
		break;
	case Tower::MACHINE_GUN:
		Range = 2;
		TowerCost = 250;
		Damage = 1;
		RateOfFire = 0.25f;
		turnAngle = -1;
		AttackType[CAN_ATTACK_GROUND] = CAN_ATTACK_GROUND;
		AttackType[CAN_ATTACK_AIR] = CAN_ATTACK_AIR;
		sprite = Sprite::create("towerDefense_tile249.png");
		sprite->setAnchorPoint(temp);
		sprite->retain();
		break;
	case Tower::SAM_SITE:
		Range = 4;
		TowerCost = 250;
		Damage = 4;
		RateOfFire = 1;
		turnAngle = -1;
		AttackType[CAN_ATTACK_AIR] = CAN_ATTACK_AIR;
		AttackType[CAN_EXPLODE_ENEMY] = CAN_EXPLODE_ENEMY;
		sprite = Sprite::create("towerDefense_tile206.png");
		sprite->setAnchorPoint(temp);
		sprite->retain();
		break;
	default:
		break;
	}
	originalRateOfFire = RateOfFire;
	canShoot = false;
	this->scheduleUpdate();
	return true;
}

void Tower::update(float delta)
{
	if (!canShoot)
	{
		if (RateOfFire <= 0)
		{
			canShoot = true;
			RateOfFire = originalRateOfFire;
		}
		else
		{
			RateOfFire -= delta;
		}
	}
}

void Tower::draw(Renderer * renderer, const Mat4 & transform, uint32_t flags)
{
	sprite->draw(renderer, transform, flags);
}

void Tower::setStats(float Range, float ROF, float dam)
{
	this->Range = Range;
	this->RateOfFire = ROF;
	this->Damage = dam;
}
void Tower::setAttackType(TowerAttackType _ground)//, TowerAttackType _air = NONE, TowerAttackType _home = NONE, TowerAttackType _explosive = NONE)
{
	bool tempbool = true;
	TowerAttackType temp[4];
	temp[0] = NONE; // used to store if tower can attack ground
	temp[1] = NONE;	// used to store if tower can attack air
	temp[2] = NONE;	// used to store if tower can home it's target
	temp[3] = NONE;	// used to store if tower have explosives

	switch (_ground)
	{
	case Tower::CAN_ATTACK_GROUND:
		temp[0] = CAN_ATTACK_GROUND;
		break;
	case Tower::CAN_ATTACK_AIR:
		temp[1] = CAN_ATTACK_AIR;
		break;
	case Tower::CAN_HOME_ENEMY:
		temp[2] = CAN_HOME_ENEMY;
		break;
	case Tower::CAN_EXPLODE_ENEMY:
		temp[3] = CAN_EXPLODE_ENEMY;
		break;
	case Tower::NONE:
	case Tower::TOWER_ATTACK_TYPE_TOTAL:
	default:
		break;
	}

	for (int i = 0; i < TOWER_ATTACK_TYPE_TOTAL; ++i)
	{
		AttackType[i] = temp[i];
	}
}

bool Tower::getAttackType(TowerAttackType _type)
{
	return (AttackType[_type] != NONE);
}

float Tower::getRange()
{
	return Range * TOWER_RANGE_MULTIPLIER;
}
float Tower::getROF()
{
	return RateOfFire;
}

bool Tower::getCanShoot()
{
	return canShoot;
}

Tower::TowerType Tower::getTowerType()
{
	return towerType;
}

void Tower::goOnCooldown()
{
	//RateOfFire = originalRateOfFire;
	canShoot = false;
}

Tower* Tower::create(const std::string & file)
{
	Tower* tower = new Tower();
	if (tower)
	{
		tower->autorelease();
		tower->init(NORMAL);
		return tower;
	}

	CC_SAFE_DELETE(tower);
	return NULL;
}

