#ifndef _GAMEMANAGER_H_
#define _GAMEMANAGER_H_

#include "cocos2d.h"

using namespace std;
using namespace cocos2d;

class CGameManager
{
private:
	static CGameManager* s_instance;

	int maxLives;
	int waveNum;
	int enemyInGame;
	int enemyCount;
	int enemyMaxCount;
	int enemyKilled;
	int bossCount;
	int bossMaxCount;
	int bossKilled;
	bool canSpawnEnemy;
	float waveSpawnDelay;
	
public:
	static CGameManager* instance();
	CGameManager();
	~CGameManager();

	int GetMaxLives();
	void SetMaxLives(int value);
	int GetWaveNum();
	void SetWaveNum(int value);
	float GetWaveSpawnDelay();
	void SetWaveSpawnDelay(float value);

	int GetEnemyInGame();
	void SetEnemyInGame(int value);
	int GetEnemyCount();
	void SetEnemyCount(int value);
	int GetEnemyMaxCount();
	void SetEnemyMaxCount(int value);
	int GetEnemyKilled();
	void SetEnemyKilled(int value);

	int GetBossCount();
	void SetBossCount(int value);
	int GetBossMaxCount();
	void SetBossMaxCount(int value);
	int GetBossKilled();
	void SetBossKilled(int value);

	bool GetCanSpawnEnemy();
	void SetCanSpawnEnemy(bool value);

	void SetParameters();
	void ResetParameters();
	void SetNewWave();
	void UpdateNumOfEnemiesKilled();
	void UpdateNumOfEnemiesSpawned();
	void UpdateNumOfBossSpawned();
	void UpdateNumOfBossKilled();
};
#endif // !_GAMEMANAGER_H_

