#include "ObjectManager.h"

CObjectManager::CObjectManager()
{
}

CObjectManager::~CObjectManager()
{
}

void CObjectManager::AddNewEnemy(CEnemy * newEnemy)
{
	enemyVector.push_back(newEnemy);
}

void CObjectManager::AddNewBullet(CBulletProjectile * newBullet)
{
	bulletVector.push_back(newBullet);
}

void CObjectManager::AddNewHoming(CHomingProjectile * newHoming)
{
	homingVector.push_back(newHoming);
}

void CObjectManager::AddNewTower(Tower* newTower)
{
	towerVector.push_back(newTower);
}

void CObjectManager::RemoveEnemy(CEnemy * removedEnemy)
{
	int removedEnemyNum = 0;
	bool isEnemyFound = false;
	for (int i = 0; i < enemyVector.size(); i++)
	{
		if ((enemyVector[i] == removedEnemy) && (isEnemyFound == false))
		{
			removedEnemyNum = i;
			isEnemyFound = true;
		}
	}

	if (isEnemyFound)
	{
		enemyVector.erase(enemyVector.begin() + (removedEnemyNum));
	}
}

void CObjectManager::RemoveBullet(CBulletProjectile * removedBullet)
{
	int removedBulletNum = 0;
	bool isBulletFound = false;
	for (int i = 0; i < bulletVector.size(); i++)
	{
		if ((bulletVector[i] == removedBullet) && (isBulletFound == false))
		{
			removedBulletNum = i;
			isBulletFound = true;
		}
	}

	if (isBulletFound)
	{
		bulletVector.erase(bulletVector.begin() + (removedBulletNum));
	}
}

void CObjectManager::RemoveHoming(CHomingProjectile * removedHoming)
{
	int removedHomingNum = 0;
	bool isHomingFound = false;
	for (int i = 0; i < homingVector.size(); i++)
	{
		if ((homingVector[i] == removedHoming) && (isHomingFound == false))
		{
			removedHomingNum = i;
			isHomingFound = true;
		}
	}

	if (isHomingFound)
	{
		homingVector.erase(homingVector.begin() + (removedHomingNum));
	}
}

void CObjectManager::RemoveTower(Tower* removedTower)
{
	int removedTowerNum = 0;
	bool isTowerFound = false;
	for (int i = 0; i < towerVector.size(); i++)
	{
		if ((towerVector[i] == removedTower) && (isTowerFound == false))
		{
			removedTowerNum = i;
			isTowerFound = true;
		}
	}

	if (isTowerFound)
	{
		towerVector.erase(towerVector.begin() + (removedTowerNum));
	}
}

int CObjectManager::getMaxSize()
{
	int bSize = bulletVector.size();
	int hSize = homingVector.size();
	return ((bSize > hSize) ? bSize : hSize);
}
