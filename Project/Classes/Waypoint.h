#ifndef _WAYPOINT_H_
#define _WAYPOINT_H_

#include "cocos2d.h"
#include <vector>

using namespace cocos2d;
using namespace std;

class CWaypoint
{
public:
	CWaypoint();
	~CWaypoint();

	void Init();
	void Destroy();
	void SetPosition(Vec2 position);
	Vec2 GetPosition(void) const;

	void SetID(const int m_iID);
	int GetID() const;

	void AddRelatedWaypoint(CWaypoint* relatedWaypoint);
	bool RemoveRelatedWaypoint(const int m_iID);
	CWaypoint* GetNearestWaypoint();
	int GetNumberOfWaypoints() const;
	

protected:
	Vec2 position;
	int m_iID;
	vector<CWaypoint*> relatedWaypoints;
};

#endif // !_WAYPOINT_H_

