/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "Enemy.h"
#include "Projectile.h"
#include "ObjectPool.h"
#include "Waypoint.h"
#include "ObjectManager.h"
#include "GameManager.h"
#include "SimpleAudioEngine.h"

using namespace cocos2d;


class HelloWorld : public cocos2d::Scene
{
public:

	CObjectPool* test;
	CGameManager* gm;
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);

	void update(float) override;
	void spawnEnemy();
	void spawnBoss();
	void onMouseMove(Event* event);
	//void onMouseMove(EventKeyboard::KeyCode keyCode, Event* event);
	bool onMouseDown(Event* event);
	void onMouseUp(Event* event);

	bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
	void onTouchEnded(cocos2d::Touch*, cocos2d::Event*);
	void onTouchMoved(cocos2d::Touch*, cocos2d::Event*);

	bool canTowerHit(CEnemy* _type, Tower* _tower);

private:
	Size playingSize;
	bool mousePressed;
	Camera* cam;
	float timer;
	float timeCount;

	CWaypointManager cWaypointManager;
	int spriteW, spriteH;
	Vec2 OriginalCameraPosition;
	Sprite* SelectionBlock;

	bool BuildMenuOpen;
	Vec2 MenuPosition;
	bool isHeldDown;
	float touchtimer;
	int towerSelected;
	bool dragging;

	// test
	int curGold;

	//int enemyInGame;
	////int waveNum;
	//int enemyCount;
	//int enemyMaxCount;
	////int maxLives;
	//int enemyKilled;
	//bool canSpawnEnemy;	

	Label* maxLivesLabel;
	Label* waveNumLabel;
	Label* waveDelayLabel;
	Label* goldLabel;

	CocosDenshion::SimpleAudioEngine* audio;

	CObjectManager objectManager;

	// global tower storage queue
	list<Tower*> towerStorage;
	list<CEnemy*> enemyStorage;
	list<CBulletProjectile*> bulletStorage;
	list<CHomingProjectile*> homingStorage;
	// i'm sorry, RangeFinder
	int RangeNormal = 4;
	int RangeMachine = 2;
	int RangeSAM = 4;
	// i'm sorry, CostFinder
	int CostNormal = 150;
	int CostMachine = 250;
	int CostSAM = 150;
	bool canBuyNormal = true;
	bool canBuyMachine = true;
	bool canBuySAM = true;

	//Size VisibleSize = Director::getVisibleSize;
};

#endif // __HELLOWORLD_SCENE_H__
