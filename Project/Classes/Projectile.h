#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "cocos2d.h"
#include "GameObject.h"
#include "Enemy.h"
using namespace cocos2d;

class CProjectileBase : public CGameObject
{
public:
	enum TARGET_TYPE
	{
		NONE = -1,
		GROUND = 0,
		AIR,
		BOTH,
	};
	CProjectileBase();
	virtual ~CProjectileBase();
	
	virtual void update(float _dt);
	bool hasBeenRemoved;
	float SpeedBuffer;
	int Damage;

	// projectile target type
	TARGET_TYPE target_type;
private:

};

class CBulletProjectile : public CProjectileBase
{
public:
	bool moveCheck;
	Vec2 targetLocation;
	float timer;

	CBulletProjectile();
	virtual ~CBulletProjectile();

	bool init();

	virtual void update(float _dt);
	virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags);

	void setData(Vec2 spawnLoc, Vec2 enemyLoc, float _speedbuffer = 1.0f);
	void SetTarget(Vec2 setTarget);
	void setTimer(float time);
	bool GetMoveCheck();
	
};

class CHomingProjectile : public CProjectileBase
{
public:
	CEnemy* locatedTarget;
	bool moveMode;

	CHomingProjectile();
	virtual ~CHomingProjectile();

	bool init();

	virtual void update(float _dt);
	virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags);

	void setData(Vec2 spawnLoc, CEnemy* enemySet, float _speedbuffer = 1.0f);
	void SetTarget(CEnemy* enemyTarget);
	//void setTimer(float time);
	//bool GetMoveCheck();

};

#endif