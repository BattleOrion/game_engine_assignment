#ifndef _OBJECTPOOL_H_
#define _OBJECTPOOL_H_
#define OBJECT_SIZE 200

#include "cocos2d.h"
#include "GameObject.h"
#include "Enemy.h"
#include "Projectile.h"
#include "base/allocator/CCAllocatorStrategyPool.h"
#include "Tower.h"

using namespace std;
using namespace cocos2d;

class CObjectPool
{
private:
	static CObjectPool* s_instance;
	//list<BulletProjectile*> freeList;
	list<CGroundEnemy*> freeGroundEnemy;
	list<CFlyingEnemy*> freeFlyingEnemy;
	list<CGroundBoss*> freeGroundBoss;
	list<CFlyingBoss*> freeFlyingBoss;
	list<CBulletProjectile*> freeBullet;
	list<CHomingProjectile*> freeHoming;
	list<Tower*> freeTower;

public:	
	static CObjectPool* instance();

	CObjectPool();
	~CObjectPool();

	//BulletProjectile* allocate();
	//CGameObject* allocate(CGameObject::GO_TYPE type);
	void initObjectPool();

	CGroundEnemy* allocateGroundEnemy();
	void releaseGroundEnemy(CGroundEnemy * _GO);

	CFlyingEnemy* allocateFlyingEnemy();
	void releaseFLyingEnemy(CFlyingEnemy * _GO);

	CGroundBoss* allocateGroundBoss();
	void releaseGroundBoss(CGroundBoss * _GO);

	CFlyingBoss* allocateFlyingBoss();
	void releaseFLyingBoss(CFlyingBoss * _GO);

	CBulletProjectile* allocateBullet();
	void releaseBullet(CBulletProjectile * _GO);

	CHomingProjectile * allocateHoming();
	void releaseHoming(CHomingProjectile * _GO);

	Tower* allocateTower();
	void releaseTower(Tower* _GO);

	int getFreeListSize();
};



#endif // !_OBJECTPOOL_H_


