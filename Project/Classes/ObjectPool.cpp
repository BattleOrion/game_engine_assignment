#include "ObjectPool.h"

CObjectPool* CObjectPool::s_instance = NULL;

CObjectPool* CObjectPool::instance()
{
	if (!s_instance)
	{
		s_instance = new CObjectPool();
	}
	return s_instance;
}

CObjectPool::CObjectPool()
{
}

CObjectPool::~CObjectPool()
{
}

void CObjectPool::initObjectPool()
{
	int sizeToFill = OBJECT_SIZE;
	if (freeGroundEnemy.size() > 0)
		sizeToFill = OBJECT_SIZE - freeGroundEnemy.size();

	for (int i = 0; i < sizeToFill; ++i)
	{
		//CGameObject* temp = new CGameObject(CGameObject::GO_ENEMY);
		CGroundEnemy* temp = new CGroundEnemy();
		temp->sprite = Sprite::create("player.png");
		temp->sprite->retain();

		temp->hpBar = Sprite::create("hpBar.png");
		temp->hpBar->retain();
		//temp->init();
		freeGroundEnemy.push_back(temp);
	}

	sizeToFill = OBJECT_SIZE;
	if (freeFlyingEnemy.size() > 0)
		sizeToFill = OBJECT_SIZE - freeFlyingEnemy.size();

	for (int i = 0; i < sizeToFill; ++i)
	{
		//CGameObject* temp = new CGameObject(CGameObject::GO_ENEMY);
		CFlyingEnemy* temp = new CFlyingEnemy();
		temp->sprite = Sprite::create("Bat_1.png");
		temp->sprite->retain();

		temp->hpBar = Sprite::create("hpBar.png");
		temp->hpBar->retain();
		//temp->init();
		freeFlyingEnemy.push_back(temp);
	}

	sizeToFill = OBJECT_SIZE;
	if (freeGroundBoss.size() > 0)
		sizeToFill = OBJECT_SIZE - freeGroundBoss.size();

	for (int i = 0; i < sizeToFill; ++i)
	{
		//CGameObject* temp = new CGameObject(CGameObject::GO_ENEMY);
		CGroundBoss* temp = new CGroundBoss();
		temp->sprite = Sprite::create("player.png");
		temp->sprite->retain();

		temp->hpBar = Sprite::create("hpBar.png");
		temp->hpBar->retain();
		//temp->init();
		freeGroundBoss.push_back(temp);
	}

	sizeToFill = OBJECT_SIZE;
	if (freeFlyingBoss.size() > 0)
		sizeToFill = OBJECT_SIZE - freeFlyingBoss.size();

	for (int i = 0; i < sizeToFill; ++i)
	{
		//CGameObject* temp = new CGameObject(CGameObject::GO_ENEMY);
		CFlyingBoss* temp = new CFlyingBoss();
		temp->sprite = Sprite::create("Bat_1.png");
		temp->sprite->retain();

		temp->hpBar = Sprite::create("hpBar.png");
		temp->hpBar->retain();
		//temp->init();
		freeFlyingBoss.push_back(temp);
	}

	sizeToFill = OBJECT_SIZE;
	if (freeBullet.size() > 0)
		sizeToFill = OBJECT_SIZE - freeBullet.size();

	for (int i = 0; i < sizeToFill; ++i)
	{
		CBulletProjectile* temp = new CBulletProjectile();
		temp->sprite = Sprite::create("towerDefense_tile272.png");
		temp->sprite->retain();
		//temp->init();
		freeBullet.push_back(temp);
	}

	sizeToFill = OBJECT_SIZE;
	if (freeHoming.size() > 0)
		sizeToFill = OBJECT_SIZE - freeHoming.size();

	for (int i = 0; i < sizeToFill; ++i)
	{
		CHomingProjectile* temp = new CHomingProjectile();
		temp->sprite = Sprite::create("rocket.png");
		temp->sprite->retain();
		//temp->init();
		freeHoming.push_back(temp);
	}

	sizeToFill = OBJECT_SIZE;
	if (freeTower.size() > 0)
		sizeToFill = OBJECT_SIZE - freeTower.size();

	for (int i = 0; i < sizeToFill; ++i)
	{
		Tower* temp = new Tower();
		temp->sprite = Sprite::create("towerDefense_tile134.png");
		temp->sprite->retain();
		temp->init();
		freeTower.push_back(temp);
	}

	//log("free objects %d", freeList.size());
}

CGroundEnemy* CObjectPool::allocateGroundEnemy()
{
	CGroundEnemy* go = new CGroundEnemy();
	if (freeGroundEnemy.size() > 0)
	{
		go = freeGroundEnemy.front();
		freeGroundEnemy.pop_front();

		return go;
	}
	else
		return NULL;
}

void CObjectPool::releaseGroundEnemy(CGroundEnemy* _GO)
{
	freeGroundEnemy.push_back(_GO);
}

CFlyingEnemy * CObjectPool::allocateFlyingEnemy()
{
	CFlyingEnemy* go = new CFlyingEnemy();
	if (freeFlyingEnemy.size() > 0)
	{
		go = freeFlyingEnemy.front();
		freeFlyingEnemy.pop_front();

		return go;
	}
	else
		return NULL;
}

void CObjectPool::releaseFLyingEnemy(CFlyingEnemy * _GO)
{
	freeFlyingEnemy.push_back(_GO);
}

CGroundBoss* CObjectPool::allocateGroundBoss()
{
	CGroundBoss* go = new CGroundBoss();
	if (freeGroundBoss.size() > 0)
	{
		go = freeGroundBoss.front();
		freeGroundBoss.pop_front();

		return go;
	}
	else
		return NULL;
}

void CObjectPool::releaseGroundBoss(CGroundBoss* _GO)
{
	freeGroundBoss.push_back(_GO);
}

CFlyingBoss* CObjectPool::allocateFlyingBoss()
{
	CFlyingBoss* go = new CFlyingBoss();
	if (freeFlyingBoss.size() > 0)
	{
		go = freeFlyingBoss.front();
		freeFlyingBoss.pop_front();

		return go;
	}
	else
		return NULL;
}

void CObjectPool::releaseFLyingBoss(CFlyingBoss* _GO)
{
	freeFlyingBoss.push_back(_GO);
}

CBulletProjectile* CObjectPool::allocateBullet()
{
	CBulletProjectile* go = new CBulletProjectile();
	if (freeBullet.size() > 0)
	{
		go = freeBullet.front();
		freeBullet.pop_front();

		return go;
	}
	else
		return NULL;
}

void CObjectPool::releaseBullet(CBulletProjectile* _GO)
{
	freeBullet.push_back(_GO);
}

CHomingProjectile* CObjectPool::allocateHoming()
{
	CHomingProjectile* go = new CHomingProjectile();
	if (freeHoming.size() > 0)
	{
		go = freeHoming.front();
		freeHoming.pop_front();

		return go;
	}
	else
		return NULL;
}

void CObjectPool::releaseHoming(CHomingProjectile* _GO)
{
	freeHoming.push_back(_GO);
}

Tower* CObjectPool::allocateTower()
{
	Tower* go = new Tower();
	if (freeTower.size() > 0)
	{
		go = freeTower.front();
		freeTower.pop_front();

		return go;
	}
	else
		return NULL;
}

void CObjectPool::releaseTower(Tower* _GO)
{
	freeTower.push_back(_GO);
}

int CObjectPool::getFreeListSize()
{
	return freeGroundEnemy.size();
}


